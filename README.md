# diffsol.jl

Differential equation solver using genetic programming, giving analytical solutions. This follows the design outlined in **[TL]**. ODEs to systems of PDEs are supported, with optional boundary conditions.

## Usage

Clone the project

```
$ git clone https://gitlab.com/johanbluecreek/diffsol.git
```

create a new file, say `example.jl`, defining your differential equation

```
$ cd diffsol
$ touch example.jl
```

For example, the following is for solving a system of PDEs containing non-linearities and potential singularities

```julia
#!/usr/bin/env julia

# include the function definitions of this project
include("./diffsol.jl")

# define variables
@vars x,y

# Define your differential equation
de = (
    # `expr` is a vector containing the trial solution, you can use SymEngine.jl's diff()
    # to specify differentiation. This is our system of differential equations:
    expr -> [
        # first equation is a PDE: ((∂_x)^2 + (∂_y)^2) Ψ_1(x,y) + (x²+y²) Ψ_1(x,y) = 0
        diff(expr[1],x,x) + diff(expr[1],y,y) - (-(x^2+y^2)*expr[1]),
        # second equation is: (∂_x Ψ_2(x,y))² + log(Ψ_2(x,y)) - cos²(x) - 2*cos(x) - 1 - log(x+sin(x)) + (Ψ_1(x,y) - sin(xy))
        diff(expr[2],x)^2 + log(expr[2]) - cos(x)^2 - 2*cos(x) - 1 - log(x+sin(x)) + (expr[1] - sin(x*y))
    ],
    # Then we define our boundary conditions, and use `safe_subs()` to specify boundaries
    expr -> [
        safe_subs(expr[1], (x,0.0)) - (0.0),
        safe_subs(expr[1], (x,1.0)) - (sin(y)),
        safe_subs(expr[1], (y,0.0)) - (0.0),
        safe_subs(expr[1], (y,1.0)) - (sin(x)),
        safe_subs(expr[2], (x,1.0)) - (1.0+sin(1.0))
    ],
    # Specify the ranges for each variable (order of variables specified later),
    # the expression will be evaluated on the product of these ranges
    [
        range(0.0,1.0,length=5), # for x
        range(0.0,1.0,length=5)  # for y
    ],
    # Ranges for the boundaries to be evaluated at, can be left out for ODEs. For
    # higher order PDEs, these can be given as `Iterators.product()` of `range()`s.
    [ range(0.0,1.0,length=10) for _ in 1:5 ],
    # Variables
    [:x,:y],
    # Number of expressions, meaning length of vector `expr` in the DE/BC definition
    2
) |> DiffEq # Make it a differential equation

# Specify options for the solver
opts = DEOpts(;
    gene_length=100,        # ~ maximum length of possible expressions, 50*(expressions in system)
                            # is reasonable, (50 default)
    population_size=1000,   # Number of trial expressions to work with. Lower is faster and less 
                            # likely to find sols
    crossover_rate=0.9,     # 1-(crossover_rate) of the worst in the population will be replaced
                            # offsprings of the crossover operation, rest are mutated
    mutation_rate=0.15,     # Chance for each entry in the gene-representation of the expression
                            # mutate
    tournament_bound=1000,  # Maximum number of individuals to partake in tournament selection
                            # for parents for crossover
    target_fit=1.0e-7,      # Fit needed for expression should be considered a solution
    crossover_chance=1.0,   # Chance for an expression to have crossover act on it
    bc_penalty=1.0          # Penalty factor to the BC calculation, changing priority over DE
                            # evaluation
)

# Run the solver
# iters specifies the number of iterations the solver should attempt before giving up
result = run(de, opts; iters=2000)

if result[2] < opts.target_fit
    println("Solution found after $(result[3]) iterations:")
    println(result[1])
else
    println("Best expression found after $(result[3]) iterations:")
    println(result[1])
    println("With fitness: $(result[2])")
end
```

Because of the nature of the algorithm and hardware, results may vary, but the solution, here being: Ψ_1(x,y) = sin(xy), Ψ_2(x,y) = Ψ_2(x) = x + sin(x); was in 30 runs found in a minimum of 19 iterations (21s) and a maximum 1489 (32m41s), with mean 275 (6m13s) and median 189 (4m27s) (times from execution on i5-8250U).

## Notes

 * Up-to-date results over the performance -- similar to **[TL]** -- is presented in `results.txt`
 * Systems and equations can be over- or under-constrained. When over-constrained some analytical approximation will be returned, possibly a solution if there is some degeneracy. When under-constrained some random solution can be found.
 * Equations does not have to be differential equations, but can be purely algebraic
 * Expression generation is made from a "grammar", and only a certain set of words and structures are available in the default grammar. The grammar can be extended by the user by editing the function `get_grammar()`. Note that to include more functions, those functions must be supported by `SymEngine.jl` or be patched to support `SymEngine.jl`s `Basic` type.
 * Algebraic constants can be included and should just be thought of as variables that the equations only depend on algebraically.
 * Hints can sometimes be given to the solver. For example solutions to the wave equation is of the form of an incoming and outgoing wave, `u(x,t) = F(x-t) + G(x+t)`, hence instead of solving for `u(t,x) = expr[1]` solve for `u(t,x) = safe_subs(expr[1] + expr[2], (t,xx-tt),(x,xx+tt),(xx,x),(tt,t))` (`SymEngine.jl` substitutes variables in left-to-right order), significantly improving performance.

## Dependencies

 * `SymEngine.jl`
 * `ProgressMeter.jl`

## Sources

 * [TL] TSOULOS, Ioannis G. et LAGARIS, Isaac E. "Solving differential equations with genetic programming". Genetic Programming and Evolvable Machines, 2006, vol. 7, no 1, p. 33-54.