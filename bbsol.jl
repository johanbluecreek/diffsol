#!/usr/bin/env julia

include("./diffsol.jl")

using BlackBoxOptim

function bbsol()
    @vars x
    de = ( # ODE3
        expr->diff(expr,x) - (-expr/5+exp(-x/5)*cos(x)),
        expr->safe_subs(expr, x=>0.0),
        range(0.0, 1.0,length=20)
    )
    #de = ( # ODE1
    #    expr->diff(expr,x) - (2*x-expr)/x,
    #    expr->safe_subs(expr, x=>0.1) - 20.1,
    #    range(0.1,1.0,length=10)
    #)
    grammar = get_grammar()
    fitter = gene -> gene_to_fit(round.(Int, gene), de, grammar)
    res = bboptimize(fitter;
        SearchRange = (0.0, 255.0), NumDimensions=50, TargetFitness=0.0,
        MaxTime=60*10, PopulationSize=200
    )
    println("Sol found:")
    best_candidate(res) |> gene -> gen_expr(round.(Int, gene), grammar) |> first |> println
end

bbsol()