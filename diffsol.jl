#!/usr/bin/env julia

using SymEngine
using StatsBase
using Unzip
using ProgressMeter

set_zero_subnormals(true)

import Base.copy

function Base.copy(sym::Symbol)
    return sym
end

macro vars_vec(x...)
    v = eval(x[1])
    q=Expr(:block)
    for s in v
        push!(q.args, Expr(:(=), esc(s), Expr(:call, :(SymEngine._symbol), Expr(:quote, s))))
    end
    push!(q.args, Expr(:tuple, v...))
    q
end

struct DEOpts
    gene_length::Int64
    population_size::Int64
    crossover_rate::Float64
    mutation_rate::Float64
    tournament_bound::Int64
    target_fit::Float64
    crossover_chance::Float64
    bc_penalty::Float64
end

function DEOpts(;
        gene_length::Int64=50,
        population_size::Int64=1000,
        crossover_rate::Float64=0.9,
        mutation_rate::Float64=0.15,
        tournament_bound::Int64=1000,
        target_fit::Float64=1.0e-7,
        crossover_chance::Float64=0.8,
        bc_penalty::Float64=100.0)
    return DEOpts(gene_length,
        population_size,
        crossover_rate,
        mutation_rate,
        tournament_bound,
        target_fit,
        crossover_chance,
        bc_penalty)
end

struct DiffEq
    de::Function
    bc::Function
    range::Iterators.ProductIterator
    bc_range::Vector{Union{StepRangeLen{Float64, Base.TwicePrecision{Float64}, Base.TwicePrecision{Float64}},Iterators.ProductIterator}}
    vars::Vector{Symbol}
    neqs::Int
end

function DiffEq(de::Function, bc::Function, ranges::Vector{StepRangeLen{Float64, Base.TwicePrecision{Float64}, Base.TwicePrecision{Float64}}}, vars::Vector{Symbol}, neqs::Int=1)
    return DiffEq(de, bc, Iterators.product(ranges...), [0.0:0.0 for _ in eachindex(ranges)], vars, neqs)
end

function DiffEq(de::Tuple{Function, Function, Vector{StepRangeLen{Float64, Base.TwicePrecision{Float64}, Base.TwicePrecision{Float64}}}, Vector{Symbol}})
    return DiffEq(de[1], de[2], Iterators.product(de[3]...), [0.0:0.0 for _ in eachindex(de[3])], de[4], 1)
end

function DiffEq(de::Tuple{Function, Function, Vector{StepRangeLen{Float64, Base.TwicePrecision{Float64}, Base.TwicePrecision{Float64}}}, Vector{Symbol}, Int})
    return DiffEq(de[1], de[2], Iterators.product(de[3]...), [0.0:0.0 for _ in eachindex(de[3])], de[4], de[5])
end

function DiffEq(de::Tuple{Function, Function, Vector{StepRangeLen{Float64, Base.TwicePrecision{Float64}, Base.TwicePrecision{Float64}}}, Vector{T} where T <: Union{StepRangeLen{Float64, Base.TwicePrecision{Float64}, Base.TwicePrecision{Float64}},Iterators.ProductIterator}, Vector{Symbol}, Int})
    return DiffEq(de[1], de[2], Iterators.product(de[3]...), de[4], de[5], de[6])
end

function DiffEq(de::Tuple{Function, Function, Vector{StepRangeLen{Float64, Base.TwicePrecision{Float64}, Base.TwicePrecision{Float64}}}, Vector{T} where T <: Union{StepRangeLen{Float64, Base.TwicePrecision{Float64}, Base.TwicePrecision{Float64}},Iterators.ProductIterator}, Vector{Symbol}})
    return DiffEq(de[1], de[2], Iterators.product(de[3]...), de[4], de[5], 1)
end

struct Population
    grammar
    de::DiffEq
    options::DEOpts
    # population data
    genes::Matrix{Int64}
    expressions::Vector{Vector{Basic}}
    depths::Vector{Vector{Int}}
    des::Vector{Vector{Basic}}
    bcs::Vector{Vector{Basic}}
    fitness::Vector{Float64}
end

function Population(
        de::DiffEq, options::DEOpts;
        bench::Bool=false
    )
    grammar = get_grammar(de.vars)
    genes = get_genes(num=options.population_size,len=options.gene_length)
    #genes[:,1] = [2,0,2,3,0,1,5,0,3,5,4] |> part -> Int[part; zeros(gene_length-length(part))] # gene with a sin(log(y-y/x)) singularity at x = 1
    #genes[:,1] = [133, 7, 218, 100, 254, 105, 212, 191, 191, 57, 58, 92, 127, 213, 10, 106, 32, 177, 54, 235] |> part -> Int[part; zeros(gene_length-length(part))] # gene with a exp(zoo+x)/exp(x) part
    #genes[:,1] = ([1,3,1,1,5,1,3,1,3,6,6,6,3,3,1,2,4,1,5] .- 1) |> part -> Int[part; zeros(options.gene_length-length(part))] # solution to PDE1
    expressions, depths = get_expressions(genes, grammar, de.neqs)
    des = de.de.(expressions)
    bcs = de.bc.(expressions)
    fits = fitness(des, bcs, de.range, de.bc_range, de.vars, options.bc_penalty)
    if bench
        # if we are doing a benchmark, we want to avoid 
        # having the solution in the starting population
        while length(findall(<(options.target_fit), fits)) > 0
            for i in findall(<(options.target_fit), fits)
                new_gene = get_genes(;num=1,len=options.gene_length)
                genes[:,i] = new_gene[:,1]
                new_e, new_d = get_expressions(new_gene, grammar, de.neqs)
                expressions[i], depths[i] = new_e[1], new_d[1]
                des[i] = de.de(expressions[i])
                bcs[i] = de.bc(expressions[i])
                fits[i] = fitness(de, expressions[i], options.bc_penalty)
            end
        end
    end

    # sort everything w.r.t. fitness
    sp = sortperm(fits)
    genes[:,:] = genes[:,sp]
    expressions[:] = expressions[sp]
    depths[:] = depths[sp]
    des[:] = des[sp]
    bcs[:] = bcs[sp]
    fits[:] = fits[sp]

    return Population(
        grammar,
        de,
        options,
        # population data
        genes,
        expressions,
        depths,
        des,
        bcs,
        fits
    )
end

function get_grammar(vars=(:x,))
    return Dict{Symbol, Vector{T} where T <: Union{Expr,Symbol,ComplexF64}}(
        :expr => Union{Symbol,Expr}[
            Expr(:call, :op, :expr, :expr),
            :expr, # (expr)
            Expr(:call, :func, :expr),
            :digit,
            vars...
        ],
        :op => Symbol[
            :+,
            :-,
            :*,
            :/
        ],
        :func => Symbol[
            :sin,
            :cos,
            :exp,
            :log
        ],
        :digit => Complex.(Float64.(0:9))
    )
end

function gen_expr(gene::Vector{Int}, grammar, symbol::Union{ComplexF64, Expr, Symbol}=:expr, depth::Int=0)
    if depth > length(gene) || (depth == length(gene) && symbol ∈ keys(grammar))
        return Complex(Inf), depth
    end
    if typeof(symbol) == Symbol
        if symbol ∈ keys(grammar)
            return gen_expr(gene, grammar, copy(grammar[symbol][(gene[depth+1] % length(grammar[symbol]))+1]), depth+1)
        end
    elseif typeof(symbol) == Expr
        if any(map(x -> x ∈ keys(grammar), symbol.args))
            for i in 1:length(symbol.args)
                if symbol.args[i] ∈ keys(grammar)
                    if depth >= length(gene)
                        return Complex(Inf), depth
                    end
                    symbol.args[i], depth = gen_expr(gene, grammar, symbol.args[i], depth)
                end
            end
        end
    end
    return symbol, depth
end

function get_genes(;num=1000,len=50)
    round.(Int, rand(len,num) .* 255)
end

function any_to_basic(x::Union{ComplexF64, Expr, Symbol})
    try
        return Basic(x) |> ret -> occursin("zoo", "$ret") ? Basic(Inf) : ret
    catch DomainError
        return Basic(Inf)
    end
end

function get_expressions(gene::Vector{Int64}, grammar)
    return gen_expr(gene, grammar) |> x -> (any_to_basic(x[1]), x[2])
end

function get_expressions(gene::Vector{Int64}, grammar, splits)
    l = floor(Int, length(gene)/splits)
    exprs = Basic[]
    depths = Int[]
    sizehint!(exprs, splits), sizehint!(depths, splits)
    for s in 1:splits
        expr, depth = get_expressions(gene[(1+(s-1)*l):(s*l)], grammar)
        push!(exprs, expr), push!(depths, depth)
    end
    return exprs, depths
end

function get_expressions(gene_matrix::Matrix{Int64}, grammar, splits::Int=1, unique=false)
    expressions = Vector{Basic}[]
    depth_collection = Vector{Int}[]
    sizehint!(expressions, size(gene_matrix)[2]), sizehint!(depth_collection, size(gene_matrix)[2])
    for i in 1:(size(gene_matrix)[2])
        exprs, depths = get_expressions(gene_matrix[:,i], grammar, splits)
        if unique
            while exprs in expressions
                gene_matrix[:,i] = get_genes(;num=1,len=size(gene_matrix)[1])[:,1]
                exprs, depths = get_expressions(gene_matrix[:,i], grammar, splits)
            end
        end
        push!(expressions, exprs), push!(depth_collection, depths)
    end
    return expressions, depth_collection
end

function get_expressions(pop::Population, idx::Int)
    get_expressions(pop.genes[:,idx], pop.grammar, pop.de.neqs)
end

function basic_to_float(x::Basic)::Float64
    try
        return Float64(x) |> x -> isnan(x) ? Inf : x
    catch ArgumentError
        return Inf
    end 
end

function safe_subs(a::Basic, S...)
    length(S) == 0 && return a
    try
        return subs(a, S...)
    catch DomainError
        return oo
    end
end

function fitness(des::Vector{Basic}, bcs::Vector{Basic}, ranges::Iterators.ProductIterator, bc_ranges::Vector{Union{StepRangeLen{Float64, Base.TwicePrecision{Float64}, Base.TwicePrecision{Float64}},Iterators.ProductIterator}}, vars=(:x,), λ::Float64=100.0)::Float64
    de_part = sum(abs2, safe_subs(de, zip(symbols.(vars), s)...) for de in des, s in ranges)
    bc_part = length(bcs) == 0 ? Basic(0.0) : λ*sum(sum(abs2, safe_subs(bcs[b], zip(free_symbols(bcs[b]), s)...) for s in bc_ranges[b]) for b in eachindex(bcs))
    return de_part + bc_part |> basic_to_float |> x -> round(x; sigdigits=12)
end

function fitness(des::Vector{Vector{Basic}}, bcs::Vector{Vector{Basic}}, ranges::Iterators.ProductIterator, bc_ranges::Vector{Union{StepRangeLen{Float64, Base.TwicePrecision{Float64}, Base.TwicePrecision{Float64}},Iterators.ProductIterator}}, vars=(:x,), λ::Float64=100.0)::Vector{Float64}
    return Float64[
        fitness(des[i], bcs[i], ranges, bc_ranges, vars, λ)
    for i in 1:length(des)]
end

function fitness(pop::Population)::Vector{Float64}
    return fitness(pop.des, pop.bcs, pop.de.range, pop.de.bc_range, pop.de.vars, pop.options.bc_penalty)
end

function fitness(de::DiffEq, expr::Vector{Basic}, λ::Float64=100.0)::Float64
    return fitness(de.de(expr), de.bc(expr), de.range, de.bc_range, de.vars, λ)
end

function gene_to_fit(gene::Vector, de::DiffEq, grammar)
    return gen_expr(gene, grammar) |> first |> any_to_basic |> expr -> fitness(de, expr)
end

function gene_to_fit(gene::Matrix, de::DiffEq, grammar)
    return Float64[
        gene_to_fit(gene[:,i], de, grammar)
    for i in 1:(size(gene)[2])]
end

function tournament(pop::Population, excludes::Vector{Int}=Int[])
    return filter(i -> i ∉ excludes && all(pop.depths[i] .> 1), 1:pop.options.population_size) |>
        set -> sample(set, rand(2:min(pop.options.tournament_bound, length(set))), replace=false, ordered=true) |>
        first
end

function crossover_parents(pop::Population)::Vector{Tuple{Int,Int}}
    c = min(round(Int,(1.0-pop.options.crossover_rate)*pop.options.population_size), length(filter(i -> all(pop.depths[i] .> 1), 1:pop.options.population_size)))
    parent_pairs = Tuple{Int,Int}[]
    sizehint!(parent_pairs, c-1)
    while length(parent_pairs) < c
        tournament(pop) |>
            p -> (p, tournament(pop, Vector{Int}(union([p], unzip(filter(ps -> p == ps[1], parent_pairs))[2])))) |>
            ps -> push!(parent_pairs, ps)
    end
    return parent_pairs
end

function crossover!(pop::Population)
    crossover_twopoint_expand!(pop)
end

function crossover_onepoint!(pop::Population)
    genes = copy(pop.genes)
    parents = crossover_parents(pop)
    for i in 1:length(parents)
        pop.genes[:,end-(i-1)] = rand(1:min(pop.depths[parents[i][1]], pop.depths[parents[i][2]])) |>
            cut -> [genes[1:cut,parents[i][1]]; genes[(cut+1):end,parents[i][2]]]
        pop.fitness[end-(i-1)] = NaN
    end
end

function crossover_twopoint!(pop::Population)
    genes = copy(pop.genes)
    parents = crossover_parents(pop)
    for i in 1:length(parents)
        pop.genes[:,end-(i-1)] = rand(1:(pop.depths[parents[i][1]]-1)) |>
            r -> (r,rand((r+1):pop.depths[parents[i][1]])) |>
            part -> [genes[1:part[1],parents[i][1]]; genes[(part[1]+1):(part[2]),parents[i][2]]; genes[(part[2]+1):end,parents[i][1]]]
        pop.fitness[end-(i-1)] = NaN
    end
end

function crossover_twopoint_expand(genes::Tuple{Vector{Int},Vector{Int}}, depths::Tuple{Int,Int})::Vector{Int}
    part1 = rand(1:(depths[1]-1)) |> r -> (r, rand((r+1):depths[1]))
    part2 = rand(1:(depths[2]-1)) |> r -> (r, rand((r+1):depths[2]))
    return [genes[1][1:part1[1]];  genes[2][(part2[1]+1):(part2[2])]; genes[1][(part1[2]+1):end]] |>
        gene -> length(gene) ≥ length(genes[1]) ? gene[1:length(genes[1])] : [gene; get_genes(;num=1,len=(length(genes[1])-length(gene)))[:,1]]
end

function crossover_twopoint_expand(genes::Matrix{Int}, parents::Tuple{Int,Int}, depths::Vector{Tuple{Int,Int}}, splits::Int, c_chance::Float64)::Vector{Int}
    l = floor(Int, size(genes)[1]/splits)
    new_gene = Int[]
    sizehint!(new_gene, size(genes)[1])
    for s in 1:splits
        if rand() < c_chance || splits == 1
            append!(new_gene, crossover_twopoint_expand((genes[:,parents[1]][(1+(s-1)*l):(s*l)], genes[:,parents[2]][(1+(s-1)*l):(s*l)]), (depths[s])))
        else
            append!(new_gene, genes[:,parents[1]][(1+(s-1)*l):(s*l)])
        end
    end
    if length(new_gene) < size(genes)[1]
        append!(new_gene, get_genes(;num=1,len=(size(genes)[1]-length(new_gene)))[:,1])
    end
    return new_gene
end

function crossover_twopoint_expand!(pop::Population)
    genes = copy(pop.genes)
    parents = crossover_parents(pop)
    @simd for i in eachindex(parents)
        pop.genes[:,end-(i-1)] = parents[i] |> par -> crossover_twopoint_expand(genes, par, zip(pop.depths[par[1]], pop.depths[par[2]]) |> collect, pop.de.neqs, pop.options.crossover_chance)
        pop.fitness[end-(i-1)] = NaN
    end
end

function mutate!(pop::Population)
    c = round(Int,(pop.options.crossover_rate)*pop.options.population_size)
    @simd for i in 1:c
        pop.genes[:,i] = [ rand() < pop.options.mutation_rate ? rand(0:255) : g for g in pop.genes[:,i]]
        pop.fitness[i] = NaN
    end
end

function update!(pop::Population)
    @simd for i in eachindex(pop.fitness)
        if isnan(pop.fitness[i])
            pop.expressions[i], pop.depths[i] = get_expressions(pop, i) # ~15% of time (?)
            pop.des[i] = pop.de.de(pop.expressions[i])
            pop.bcs[i] = pop.de.bc(pop.expressions[i])
            pop.fitness[i] = fitness(pop.des[i], pop.bcs[i], pop.de.range, pop.de.bc_range, pop.de.vars, pop.options.bc_penalty) # ~75-80% time
        end
    end
    sort_pop!(pop)
end

function sort_pop!(pop::Population)
    sp = sortperm(pop.fitness)
    pop.genes[:,:] = pop.genes[:,sp]
    pop.expressions[:] = pop.expressions[sp]
    pop.depths[:] = pop.depths[sp]
    pop.des[:] = pop.des[sp]
    pop.bcs[:] = pop.bcs[sp]
    pop.fitness[:] = pop.fitness[sp]
end

function iterate!(pop::Population)
    crossover!(pop)
    mutate!(pop)
    update!(pop) # ~97% of time
end

function random_run(de; target_fit=1e-7, iters=2000)
    grammar = get_grammar()
    genes = get_genes(num=1000,len=50)
    fits = gene_to_fit(genes, de, grammar)
    best = findmin(fits) |> m -> (gen_expr(genes[:,m[2]], grammar) |> first |> any_to_basic, m[1], 0)
    println("(0) Best is: $(best[1]); with fitness=$(best[2])")
    @showprogress "Solving..." for i in 1:iters
        genes = get_genes(num=1000,len=50)
        fits = gene_to_fit(genes, de, grammar)
        current_best = findmin(fits) |> m -> (gen_expr(genes[:,m[2]], grammar) |> first |> any_to_basic, m[1], i)
        if current_best[2] < best[2]
            best = current_best
            println("\n($i) Best is: $(best[1]); with fitness=$(best[2])")
        end
        if best[2] < target_fit
            println("\nSolution found. Exiting.")
            return best
        end
    end
    println("Solution not found.")
    return best
end

function run(de, opt; iters=2000, bench=false)
    pop = Population(de, opt; bench=bench)
    best = (pop.expressions[1], pop.fitness[1], 0)
    println("(0) Best is: $(best[1]); with fitness=$(best[2]) (mean top-10 fitness=$(round(mean( filter(<(Inf), pop.fitness) |> fits -> fits[1:min(10,length(fits))] ); sigdigits=4)))")
    @showprogress "Solving..." for i in 1:iters
        iterate!(pop)
        if pop.fitness[1] < best[2]
            best = (pop.expressions[1],pop.fitness[1], i)
            println("\n($i) Best is: $(best[1]); with fitness=$(best[2]) (mean top-10 fitness=$(round(mean( filter(<(Inf), pop.fitness) |> fits -> fits[1:min(10,length(fits))] ); sigdigits=4)))")
        end
        if pop.fitness[1] < pop.options.target_fit
            println("\nSolution found. Exiting.")
            return best
        end
    end
    println("Solution not found.")
    return best
end

