#!/usr/bin/env julia

include("./diffsol.jl")

function print_stats(de_list, stats, target_fit)
    for i in 1:length(stats)
        fns = [SymFunction("Ψ_$j")(symbols.(de_list[i].vars)...) for j in 1:9]
        println("================")
        println("For the de:")
        for e in de_list[i].de(fns)
            println(e)
        end
        println("and bc:")
        for e in de_list[i].bc(fns)
            println(e)
        end
        exprs, fits, iters = unzip(stats[i])
        iters = iters[findall(<(target_fit), fits)]
        if length(iters) > 0
            println("Stats:\n min=$(min(iters...))\n max=$(max(iters...))\n mean=$(round(Int, mean(iters)))\n failures=$(length(exprs)-length(iters)), successes=$(length(iters))")
        else
            println("Stats:\n failures=$(length(exprs)-length(iters))")
        end
    end
    println("================")
end

function benchmark()
    @vars x, y, z
    de_list = [
        (( # ODE1
            expr->[diff(expr[1],x) - (2*x-expr[1])/x],
            expr->[safe_subs(expr[1], x=>0.1) - 20.1],
            [range(0.1,1.0,length=10)],
            [:x]
        ),
            DEOpts()
        ),
        (( # ODE2
            expr->[diff(expr[1],x) - ((1-expr[1]*cos(x))/sin(x))],
            expr->[safe_subs(expr[1], x=>0.1) - 2.1/sin(0.1)],
            [range(0.1,1.0,length=10)],
            [:x]
        ),
            DEOpts()
        ),
        (( # ODE3
            expr->[diff(expr[1],x) - (-expr[1]/5+exp(-x/5)*cos(x))],
            expr->[safe_subs(expr[1], x=>0.0)],
            [range(0.0, 1.0,length=20)],
            [:x]
        ),
            DEOpts()
        ),
        (( # ODE4
            expr->[diff(expr[1],x,x) + 100*expr[1]],
            expr->[safe_subs(expr[1], x=>0.0), safe_subs(diff(expr, x), x=>0.0) - 10.0],
            [range(0.0, 1.0,length=10)],
            [:x]
        ),
            DEOpts()
        ),
        (( # ODE5
            expr->[diff(expr[1],x,x) - (6*diff(expr[1], x)-9*expr[1])],
            expr->[safe_subs(expr[1], x=>0.0), safe_subs(diff(expr, x), x=>0.0)-2.0],
            [range(0.0, 2.0,length=10)],
            [:x]
        ),
            DEOpts()
        ),
        (( # ODE6
            expr->[diff(expr[1],x,x) - (-diff(expr[1],x)/5-expr[1]-exp(-x/5)*cos(x)/5)],
            expr->[safe_subs(expr[1], x=>0.0) - 0.0, safe_subs(diff(expr[1], x), x=>0.0)-1.0],
            [range(0.0, 2.0,length=20)],
            [:x]
        ),
            DEOpts()
        ),
        (( # ODE7
            expr->[diff(expr[1],x,x) + 100*expr[1]],
            expr->[safe_subs(expr[1], x=>0.0), safe_subs(expr[1], x=>1.0) - sin(10.0)],
            [range(0.0, 1.0,length=10)],
            [:x]
        ),
            DEOpts()
        ),
        (( # ODE8
            expr->[x*diff(expr[1],x,x) + (1-x)*diff(expr[1], x) + expr[1]],
            expr->[safe_subs(expr[1], x=>0.0)-1.0, safe_subs(expr, x=>1.0)],
            [range(0.0, 1.0,length=10)],
            [:x]
        ),
            DEOpts()
        ),
        (( # ODE9
            expr->[diff(expr[1],x,x) - (-diff(expr[1],x)/5-expr[1]-exp(-x/5)*cos(x)/5)],
            expr->[safe_subs(expr[1], x=>0.0) - 0.0, safe_subs(expr[1], x=>1.0)-sin(1.0)/exp(0.2)],
            [range(0.0,1.0,length=20)],
            [:x]
        ),
            DEOpts()
        ),
        (( # NLODE1
            expr->[diff(expr[1],x)-1/(2*expr[1])],
            expr->[safe_subs(expr,x=>1.0)-1.0],
            [range(1.0,4.0,length=10)],
            [:x]
        ),
            DEOpts()
        ),
        (( # NLODE2
            expr->[diff(expr[1],x)^2 + log(expr[1]) - cos(x)^2 - 2*cos(x) - 1 - log(x+sin(x))],
            expr->[safe_subs(expr[1],x=>1.0)-(1.0+sin(1.0))],
            [range(1.0,2.0,length=10)],
            [:x]
        ),
            DEOpts()
        ),
        (( # NLODE3
            expr->[diff(expr[1],x,x)*diff(expr[1],x)-(-4/x^3)],
            expr->[safe_subs(expr[1],x=>1.0)],
            [range(1.0,2.0,length=10)],
            [:x]
        ),
            DEOpts()
        ),
        (( # NLODE4
            expr->[x^2*diff(expr[1],x,x) + (x*diff(expr[1],x))^2 + 1/log(x)],
            expr->[safe_subs(expr[1], x=>exp(1.0)), safe_subs(diff(expr[1],x),x=>exp(1.0))-exp(-1.0)],
            [range(exp(1.0),2*exp(1.0),length=10)],
            [:x]
        ),
            DEOpts()
        ),
        (( # SODE1
            expr->[diff(expr[1], x) - (cos(x) + expr[1]^2 + expr[2] - (x^2 + sin(x)^2)), diff(expr[2], x) - (2*x-x^2*sin(x)+expr[1]*expr[2])],
            expr->[safe_subs(expr[1], (x,0.0)), safe_subs(expr[2], (x,0.0))],
            [range(0.0,1.0,length=20)],
            [:x],
            2
        ),
            DEOpts(;gene_length=100)
        ),
        (( # SODE2
            expr->[diff(expr[1], x) - (cos(x)-sin(x))/expr[2], diff(expr[2], x) - (expr[1]*expr[2] + exp(x) - sin(x))],
            expr->[safe_subs(expr[1],(x,0.0)) - 0.0, safe_subs(expr[2], (x, 0.0)) - 1.0],
            [range(0.0,1.0,length=20)],
            [:x],
            2
        ),
            DEOpts(;gene_length=100)
        ),
        (( # SODE3
            expr->[diff(expr[1], x) - cos(x), diff(expr[2],x) - (-expr[1]), diff(expr[3], x) - expr[2], diff(expr[4], x) - (-expr[3]), diff(expr[5], x) - expr[4]],
            expr->[safe_subs(expr[1], (x,0.0)), safe_subs(expr[2], (x,0.0)) - 1.0, safe_subs(expr[3], (x,0.0)), safe_subs(expr[4], (x,0.0)) - 1.0, safe_subs(expr[5], (x,0.0))],
            [range(0.0,1.0,length=20)],
            [:x],
            5
        ),
            DEOpts(;gene_length=5*50)
        ),
        (( # SODE4
            expr->[diff(expr[1], x) - (-1/expr[2]*sin(exp(x))), diff(expr[2], x) - (-expr[2])],
            expr->[safe_subs(expr[1], (x,0.0))-cos(1.0), safe_subs(expr[2], (x,0.0))-1.0],
            [range(0.0,1.0,length=20)],
            [:x],
            2
        ),
            DEOpts(;gene_length=100)
        ),
        (( # PDE1
            expr->[diff(expr[1],x,x) + diff(expr[1],y,y) - (exp(-x)*(x-2+y^3+6*y))],
            expr->[safe_subs(expr[1], (x,0.0)) - y^3, safe_subs(expr[1], (x,1.0)) - (1+y^3)*exp(-1.0), safe_subs(expr[1], (y,0.0)) - (x*exp(-x)), safe_subs(expr[1], (y,1.0)) - ((x+1)*exp(-x)) ],
            [range(0.0,1.0,length=5), range(0.0,1.0,length=5)],
            [ range(0.0,1.0,length=10) for _ in 1:4 ],
            [:x,:y],
            1
        ),
            DEOpts(;bc_penalty=1.0,gene_length=100)
        ),
        (( # PDE2
            expr->[diff(expr[1],x,x) + diff(expr[1],y,y) - (-2*expr[1])],
            expr->[
                safe_subs(expr[1], (x,0.0)) - 0.0,
                safe_subs(expr[1], (x,1.0)) - sin(1.0)*cos(y),
                safe_subs(expr[1], (y,0.0)) - sin(x),
                safe_subs(expr[1], (y,1.0)) - sin(x)*cos(1.0)
            ],
            [range(0.0,1.0,length=5), range(0.0,1.0,length=5)],
            [ range(0.0,1.0,length=10) for _ in 1:4 ],
            [:x,:y],
            1
        ),
            DEOpts(;bc_penalty=1.0,gene_length=100)
        ),
        (( # PDE3
            expr->[diff(expr[1],x,x) + diff(expr[1],y,y) - 4],
            expr->[
                safe_subs(expr[1], (x,0.0)) - (y^2+y+1),
                safe_subs(expr[1], (x,1.0)) - (y^2+y+3),
                safe_subs(expr[1], (y,0.0)) - (x^2+x+1),
                safe_subs(expr[1], (y,1.0)) - (x^2+x+3)
            ],
            [range(0.0,1.0,length=5), range(0.0,1.0,length=5)],
            [ range(0.0,1.0,length=10) for _ in 1:4 ],
            [:x,:y],
            1
        ),
            DEOpts(;bc_penalty=1.0,gene_length=100)
        ),
        (( # PDE4
            expr->[diff(expr[1],x,x) + diff(expr[1],y,y) - (-(x^2+y^2)*expr[1])],
            expr->[
                safe_subs(expr[1], (x,0.0)) - (0.0),
                safe_subs(expr[1], (x,1.0)) - (sin(y)),
                safe_subs(expr[1], (y,0.0)) - (0.0),
                safe_subs(expr[1], (y,1.0)) - (sin(x))
            ],
            [range(0.0,1.0,length=5), range(0.0,1.0,length=5)],
            [ range(0.0,1.0,length=10) for _ in 1:4 ],
            [:x,:y],
            1
        ),
            DEOpts(;bc_penalty=10.0,gene_length=100)
        ),
        (( # PDE5
            expr->[diff(expr[1],x,x) + diff(expr[1],y,y) - ((x-2)*exp(-x) + x*exp(-y))],
            expr->[
                safe_subs(expr[1], (y,0.0)) - (x*(exp(-x)+1)),
                safe_subs(expr[1], (y,1.0)) - (x*(exp(-x)+exp(-1))),
                safe_subs(expr[1], (x,0.0)) - (0.0),
                safe_subs(expr[1], (x,1.0)) - (exp(-y)+exp(-1))
            ],
            [range(0.0,1.0,length=5), range(0.0,1.0,length=5)],
            [ range(0.0,1.0,length=10) for _ in 1:4 ],
            [:x,:y],
            1
        ),
            DEOpts(;bc_penalty=1.0,gene_length=100)
        ),
        (( # PDE6
            expr->[diff(expr[1],x,x) + diff(expr[1],y,y) + exp(expr[1]) - (1+x^2+y^2+4/(1+x^2+y^2)^2)],
            expr->[
                safe_subs(expr[1], (x,0.0)) - (log(1+y^2)),
                safe_subs(expr[1], (x,1.0)) - (log(2+y^2)),
                safe_subs(expr[1], (y,0.0)) - (log(1+x^2)),
                safe_subs(expr[1], (y,1.0)) - (log(2+x^2))
            ],
            [range(-1.0,1.0,length=5), range(-1.0,1.0,length=5)],
            [ range(0.0,1.0,length=10) for _ in 1:4 ],
            [:x,:y],
            1
        ),
            DEOpts(;bc_penalty=1.0,gene_length=100)
        ),
        (( # PDE7
            expr->[diff(expr[1],x,x) + diff(expr[1],y,y) + diff(expr[1],z,z) - (6)],
            expr->[
                safe_subs(expr[1], (x,0.0)) - (y^2+z^2+1),
                safe_subs(expr[1], (x,1.0)) - (y^2+z^2+2),
                safe_subs(expr[1], (y,0.0)) - (x^2+z^2+1),
                safe_subs(expr[1], (y,1.0)) - (x^2+z^2+2),
                safe_subs(expr[1], (z,0.0)) - (x^2+y^2+1),
                safe_subs(expr[1], (z,1.0)) - (x^2+y^2+2)
            ],
            [range(0.0,1.0,length=4), range(0.0,1.0,length=4), range(0.0,1.0,length=4)],
            [ Iterators.product(range(0.0,1.0,length=4), range(0.0,1.0,length=4)) for _ in 1:6 ],
            [:x,:y,:z],
            1
        ),
            DEOpts(;bc_penalty=1.0,gene_length=100)
        )
    ]
    de_list, opt_list = unzip(de_list)
    de_list = DiffEq.(de_list)

    loops = 30
    iters = 2000

    stats = []

    for i in 1:length(de_list)
        fns = [SymFunction("Ψ_$j")(symbols.(de_list[i].vars)...) for j in 1:9]
        de = de_list[i]
        opt = opt_list[i]
        println("Solving:")
        for e in de.de(fns)
            println(e)
        end
        bests = []
        for j in 1:loops
            result = run(de, opt; iters=iters, bench=true)
            push!(bests, result)
            println("Completed run: $j")
        end
        push!(stats, bests)
        print_stats(de_list, stats, opt.target_fit)
    end
    println("End of benchmarks.")
end

benchmark()
